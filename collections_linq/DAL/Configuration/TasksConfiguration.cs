﻿using Bogus;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace DAL.Configuration
{
    public class TasksConfiguration : IEntityTypeConfiguration<Tasks>
    {
        public void Configure(EntityTypeBuilder<Tasks> builder)
        {
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);
            builder.Property(e => e.Description)
                .HasMaxLength(200);
            builder.HasOne(e => e.Project)
                .WithMany(e => e.Tasks)
                .HasForeignKey(e => e.ProjectId)
                .OnDelete(DeleteBehavior.SetNull);
            builder.HasIndex(e => e.PerformerId);
            builder.Property(e => e.StateId)
                .HasDefaultValue(1);
            builder.HasCheckConstraint("CK_Tasks_CreatedAt", "[CreatedAt] <= getdate()");

            var rendom = new Random();
            var id = 1;
            var fake = new Faker<Tasks>()
                .RuleFor(t => t.Id, f => id++)
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.PerformerId, f => f.Random.Int(1, 30))
                .RuleFor(t => t.ProjectId, f => f.Random.Int(1, 10))
                .RuleFor(t => t.StateId, f => f.Random.Int(1, 4))
                .RuleFor(t => t.Description, f => f.Lorem.Sentence(rendom.Next(5, 15)))
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2017, 1, 1), new DateTime(2018, 12, 12)))
                .RuleFor(t => t.FinishedAt, f => f.Date.Between(new DateTime(2017, 1, 1), DateTime.Now));
            var basicData = Enumerable.Range(1, 50)
                .Select(e => fake.Generate());

            builder.HasData(basicData);
        }
    }
}
