﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Base
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : BaseModel
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> GetById(int id);
        Task Add(IEnumerable<TEntity> entities);
        Task Add(TEntity entity);
        void Remove(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void Update(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
    }
}
