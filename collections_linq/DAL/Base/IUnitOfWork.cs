﻿using DAL.Models;
using System.Threading.Tasks;

namespace DAL.Base
{
    public interface IUnitOfWork
    {
        IRepository<Project> ProjectRepository { get; }
        IRepository<Tasks> TaskRepository { get; }
        IRepository<Team> TeamRepository { get; }
        IRepository<User> UserRepository { get; }

        Task SaveChanges();
    }
}