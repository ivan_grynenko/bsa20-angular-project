﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class User : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public List<Tasks> Tasks { get; set; }
        public List<Project> Projects { get; set; }
    }
}
