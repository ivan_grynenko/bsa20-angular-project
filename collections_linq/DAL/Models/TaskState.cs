﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class TaskState : BaseModel
    {
        public string Name { get; set; }
        public List<Tasks> Tasks { get; set; }
    }
}
