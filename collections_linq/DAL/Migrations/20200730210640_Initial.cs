﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskStates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamName = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                    table.CheckConstraint("CK_Teams_CreatedAt", "[CreatedAt] <= getdate()");
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Birthday = table.Column<DateTime>(nullable: true),
                    RegisteredAt = table.Column<DateTime>(nullable: false),
                    TeamId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.UniqueConstraint("AK_Users_Email", x => x.Email);
                    table.CheckConstraint("CK_Users_Birthday", "[Birthday] <= getdate()");
                    table.CheckConstraint("CK_Users_RegisteredAt", "[RegisteredAt] <= getdate()");
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Deadline = table.Column<DateTime>(nullable: true),
                    AuthorId = table.Column<int>(nullable: true),
                    TeamId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.CheckConstraint("CK_Projects_CreatedAt", "[CreatedAt] <= getdate()");
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    FinishedAt = table.Column<DateTime>(nullable: true),
                    StateId = table.Column<int>(nullable: false, defaultValue: 1),
                    ProjectId = table.Column<int>(nullable: true),
                    PerformerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.CheckConstraint("CK_Tasks_CreatedAt", "[CreatedAt] <= getdate()");
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Tasks_TaskStates_StateId",
                        column: x => x.StateId,
                        principalTable: "TaskStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "TeamName" },
                values: new object[,]
                {
                    { 1, new DateTime(2016, 4, 23, 10, 24, 43, 646, DateTimeKind.Unspecified).AddTicks(7875), "Team A" },
                    { 2, new DateTime(2016, 7, 29, 16, 41, 37, 572, DateTimeKind.Unspecified).AddTicks(1445), "Team B" },
                    { 3, new DateTime(2018, 4, 22, 15, 2, 59, 606, DateTimeKind.Unspecified).AddTicks(3380), "Team C" },
                    { 4, new DateTime(2017, 5, 20, 18, 40, 23, 709, DateTimeKind.Unspecified).AddTicks(6601), "Team D" },
                    { 5, new DateTime(2015, 8, 16, 1, 59, 3, 318, DateTimeKind.Unspecified).AddTicks(8743), "Team E" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 26, new DateTime(2008, 8, 20, 18, 19, 24, 440, DateTimeKind.Unspecified).AddTicks(1440), "Austin.Wisozk54@hotmail.com", "Austin", "Wisozk", new DateTime(2017, 7, 6, 7, 48, 27, 927, DateTimeKind.Unspecified).AddTicks(4125), null },
                    { 25, new DateTime(1960, 1, 19, 8, 50, 18, 538, DateTimeKind.Unspecified).AddTicks(3528), "Scarlett_Conroy9@hotmail.com", "Scarlett", "Conroy", new DateTime(2020, 2, 27, 7, 29, 50, 709, DateTimeKind.Unspecified).AddTicks(8098), null },
                    { 22, new DateTime(2002, 1, 2, 19, 27, 16, 88, DateTimeKind.Unspecified).AddTicks(6706), "Antonetta_Zemlak@yahoo.com", "Antonetta", "Zemlak", new DateTime(2020, 1, 12, 16, 56, 3, 432, DateTimeKind.Unspecified).AddTicks(5656), null },
                    { 20, new DateTime(1992, 10, 27, 5, 24, 20, 344, DateTimeKind.Unspecified).AddTicks(9078), "Nelle_Willms41@hotmail.com", "Nelle", "Willms", new DateTime(2016, 10, 23, 3, 25, 8, 394, DateTimeKind.Unspecified).AddTicks(3901), null },
                    { 18, new DateTime(2006, 4, 18, 22, 8, 8, 564, DateTimeKind.Unspecified).AddTicks(604), "Maurine.Kiehn@yahoo.com", "Maurine", "Kiehn", new DateTime(2020, 2, 10, 10, 20, 4, 731, DateTimeKind.Unspecified).AddTicks(8948), null },
                    { 17, new DateTime(1979, 11, 30, 5, 11, 23, 902, DateTimeKind.Unspecified).AddTicks(1263), "Bennie50@gmail.com", "Bennie", "Veum", new DateTime(2014, 1, 9, 5, 14, 16, 614, DateTimeKind.Unspecified).AddTicks(9094), null },
                    { 16, new DateTime(1963, 11, 8, 23, 30, 10, 271, DateTimeKind.Unspecified).AddTicks(4650), "Elouise.Erdman82@gmail.com", "Elouise", "Erdman", new DateTime(2016, 6, 19, 20, 13, 10, 933, DateTimeKind.Unspecified).AddTicks(3482), null },
                    { 8, new DateTime(1984, 10, 14, 1, 24, 54, 35, DateTimeKind.Unspecified).AddTicks(4982), "Lonnie_OHara@gmail.com", "Lonnie", "O'Hara", new DateTime(2020, 5, 22, 11, 49, 18, 188, DateTimeKind.Unspecified).AddTicks(3271), null },
                    { 13, new DateTime(1977, 8, 2, 8, 49, 32, 529, DateTimeKind.Unspecified).AddTicks(5498), "Harvey_Wiegand60@gmail.com", "Harvey", "Wiegand", new DateTime(2010, 5, 3, 13, 43, 39, 431, DateTimeKind.Unspecified).AddTicks(7160), null },
                    { 12, new DateTime(2009, 6, 1, 13, 35, 24, 717, DateTimeKind.Unspecified).AddTicks(5132), "Rosetta.Balistreri45@hotmail.com", "Rosetta", "Balistreri", new DateTime(2015, 2, 3, 2, 20, 43, 500, DateTimeKind.Unspecified).AddTicks(4241), null },
                    { 11, new DateTime(1963, 9, 13, 0, 15, 40, 603, DateTimeKind.Unspecified).AddTicks(9510), "Donnie.Gislason@hotmail.com", "Donnie", "Gislason", new DateTime(2014, 12, 11, 2, 18, 35, 162, DateTimeKind.Unspecified).AddTicks(3517), null },
                    { 28, new DateTime(1964, 12, 24, 7, 33, 50, 355, DateTimeKind.Unspecified).AddTicks(3514), "Eugenia.Jenkins58@gmail.com", "Eugenia", "Jenkins", new DateTime(2014, 1, 31, 14, 46, 59, 545, DateTimeKind.Unspecified).AddTicks(4082), null },
                    { 5, new DateTime(1996, 12, 2, 1, 11, 45, 515, DateTimeKind.Unspecified).AddTicks(8560), "Kari_Koch61@yahoo.com", "Kari", "Koch", new DateTime(2019, 1, 12, 7, 19, 32, 121, DateTimeKind.Unspecified).AddTicks(1592), null },
                    { 3, new DateTime(1996, 9, 27, 3, 47, 31, 925, DateTimeKind.Unspecified).AddTicks(8368), "River99@hotmail.com", "River", "Keeling", new DateTime(2013, 11, 8, 0, 38, 12, 717, DateTimeKind.Unspecified).AddTicks(5784), null },
                    { 1, new DateTime(1982, 2, 17, 21, 59, 50, 658, DateTimeKind.Unspecified).AddTicks(7343), "Della.Jerde11@yahoo.com", "Della", "Jerde", new DateTime(2017, 10, 15, 8, 34, 14, 40, DateTimeKind.Unspecified).AddTicks(3189), null },
                    { 15, new DateTime(1999, 2, 28, 8, 57, 33, 820, DateTimeKind.Unspecified).AddTicks(6028), "Thalia97@gmail.com", "Thalia", "Bernier", new DateTime(2020, 2, 22, 2, 11, 28, 688, DateTimeKind.Unspecified).AddTicks(2528), null },
                    { 29, new DateTime(1993, 9, 23, 6, 21, 42, 904, DateTimeKind.Unspecified).AddTicks(7520), "Mara_Hane@yahoo.com", "Mara", "Hane", new DateTime(2017, 1, 3, 6, 50, 43, 93, DateTimeKind.Unspecified).AddTicks(481), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 10, 22, new DateTime(2018, 6, 29, 1, 57, 48, 647, DateTimeKind.Unspecified).AddTicks(3194), new DateTime(2020, 1, 7, 13, 23, 0, 718, DateTimeKind.Unspecified).AddTicks(8527), "Laudantium perspiciatis et sint inventore harum sit animi dicta tempore magni assumenda voluptatum.", "Grove", 3 },
                    { 1, 17, new DateTime(2019, 8, 29, 5, 43, 26, 332, DateTimeKind.Unspecified).AddTicks(137), new DateTime(2021, 7, 31, 6, 47, 48, 400, DateTimeKind.Unspecified).AddTicks(2245), "Ut laboriosam voluptas et veniam repudiandae laudantium quibusdam consequatur.", "Cambridgeshire", 5 },
                    { 4, 15, new DateTime(2018, 8, 7, 19, 9, 50, 465, DateTimeKind.Unspecified).AddTicks(2529), new DateTime(2021, 5, 17, 3, 29, 29, 253, DateTimeKind.Unspecified).AddTicks(5757), "Molestias est aliquid animi omnis ad quod commodi nemo facilis cumque optio.", "HTTP", 1 },
                    { 5, 3, new DateTime(2018, 7, 17, 17, 38, 47, 605, DateTimeKind.Unspecified).AddTicks(2011), new DateTime(2020, 2, 20, 5, 57, 36, 68, DateTimeKind.Unspecified).AddTicks(5436), "Rem incidunt quod eveniet voluptatem nam incidunt molestiae rerum quo expedita non consequuntur.", "olive", 5 },
                    { 3, 20, new DateTime(2017, 12, 18, 7, 30, 49, 617, DateTimeKind.Unspecified).AddTicks(4461), new DateTime(2021, 3, 15, 15, 43, 23, 243, DateTimeKind.Unspecified).AddTicks(4802), "Iusto enim totam qui accusantium aut ipsam vel.", "SMS", 1 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 24, new DateTime(1969, 10, 8, 8, 48, 22, 701, DateTimeKind.Unspecified).AddTicks(4030), "Lavinia16@yahoo.com", "Lavinia", "Crist", new DateTime(2016, 2, 27, 10, 51, 35, 309, DateTimeKind.Unspecified).AddTicks(4816), 5 },
                    { 21, new DateTime(1964, 11, 27, 10, 46, 17, 803, DateTimeKind.Unspecified).AddTicks(6060), "Malika72@yahoo.com", "Malika", "Sauer", new DateTime(2016, 5, 29, 16, 27, 57, 762, DateTimeKind.Unspecified).AddTicks(8808), 5 },
                    { 30, new DateTime(1986, 10, 21, 1, 19, 8, 31, DateTimeKind.Unspecified).AddTicks(7019), "Carissa_Ritchie@gmail.com", "Carissa", "Ritchie", new DateTime(2014, 5, 26, 8, 13, 7, 884, DateTimeKind.Unspecified).AddTicks(5325), 4 },
                    { 23, new DateTime(2004, 10, 9, 20, 44, 20, 880, DateTimeKind.Unspecified).AddTicks(8074), "Bailee.Schuster42@gmail.com", "Bailee", "Schuster", new DateTime(2012, 12, 4, 7, 17, 53, 771, DateTimeKind.Unspecified).AddTicks(3857), 4 },
                    { 14, new DateTime(2009, 2, 7, 6, 41, 12, 126, DateTimeKind.Unspecified).AddTicks(4534), "Santina42@hotmail.com", "Santina", "Doyle", new DateTime(2019, 4, 28, 1, 17, 17, 538, DateTimeKind.Unspecified).AddTicks(7082), 4 },
                    { 6, new DateTime(1994, 9, 13, 16, 16, 40, 123, DateTimeKind.Unspecified).AddTicks(5774), "Josefa84@hotmail.com", "Josefa", "Hoppe", new DateTime(2016, 4, 14, 5, 37, 17, 77, DateTimeKind.Unspecified).AddTicks(7165), 4 },
                    { 4, new DateTime(2008, 4, 7, 3, 52, 28, 663, DateTimeKind.Unspecified).AddTicks(8372), "Timmothy.Pollich@hotmail.com", "Timmothy", "Pollich", new DateTime(2020, 1, 31, 21, 23, 52, 792, DateTimeKind.Unspecified).AddTicks(1352), 4 },
                    { 27, new DateTime(1972, 7, 25, 16, 33, 6, 320, DateTimeKind.Unspecified).AddTicks(9242), "Casper97@yahoo.com", "Casper", "Frami", new DateTime(2015, 3, 22, 16, 16, 16, 694, DateTimeKind.Unspecified).AddTicks(2991), 3 },
                    { 10, new DateTime(1976, 7, 25, 13, 52, 54, 475, DateTimeKind.Unspecified).AddTicks(62), "Jermey.Upton39@hotmail.com", "Jermey", "Upton", new DateTime(2011, 4, 26, 19, 52, 46, 438, DateTimeKind.Unspecified).AddTicks(9867), 3 },
                    { 2, new DateTime(1983, 12, 3, 17, 9, 54, 27, DateTimeKind.Unspecified).AddTicks(9608), "Dalton14@yahoo.com", "Dalton", "Tremblay", new DateTime(2010, 5, 30, 16, 13, 25, 741, DateTimeKind.Unspecified).AddTicks(8649), 3 },
                    { 7, new DateTime(2000, 6, 7, 12, 54, 24, 361, DateTimeKind.Unspecified).AddTicks(8770), "Dortha.Kessler42@gmail.com", "Dortha", "Kessler", new DateTime(2011, 1, 29, 1, 0, 11, 805, DateTimeKind.Unspecified).AddTicks(9572), 2 },
                    { 9, new DateTime(2007, 4, 7, 16, 38, 34, 160, DateTimeKind.Unspecified).AddTicks(3450), "Mathilde63@yahoo.com", "Mathilde", "Bins", new DateTime(2012, 5, 13, 4, 0, 43, 547, DateTimeKind.Unspecified).AddTicks(2826), 4 },
                    { 19, new DateTime(1996, 3, 9, 12, 3, 32, 964, DateTimeKind.Unspecified).AddTicks(9484), "Ole_Daniel30@gmail.com", "Ole", "Daniel", new DateTime(2017, 11, 12, 20, 11, 39, 475, DateTimeKind.Unspecified).AddTicks(9994), 1 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 9, 10, new DateTime(2019, 9, 2, 11, 19, 12, 587, DateTimeKind.Unspecified).AddTicks(3434), new DateTime(2020, 9, 23, 7, 48, 58, 455, DateTimeKind.Unspecified).AddTicks(1939), "Aspernatur voluptatibus labore nisi sint voluptatem tempore asperiores maxime sed et architecto error.", "Human", 2 },
                    { 6, 27, new DateTime(2018, 9, 14, 18, 38, 12, 843, DateTimeKind.Unspecified).AddTicks(2375), new DateTime(2021, 4, 3, 22, 18, 8, 32, DateTimeKind.Unspecified).AddTicks(1430), "Minima non necessitatibus repudiandae quasi sapiente dolorem placeat sit.", "Pines", 2 },
                    { 8, 4, new DateTime(2019, 10, 22, 16, 45, 24, 983, DateTimeKind.Unspecified).AddTicks(1019), new DateTime(2020, 8, 27, 20, 54, 50, 785, DateTimeKind.Unspecified).AddTicks(6591), "Dicta repellat possimus harum ratione culpa ipsam libero.", "Cocos (Keeling) Islands", 4 },
                    { 2, 6, new DateTime(2019, 6, 5, 22, 1, 39, 283, DateTimeKind.Unspecified).AddTicks(8543), new DateTime(2020, 6, 24, 6, 46, 48, 907, DateTimeKind.Unspecified).AddTicks(1656), "Ipsum dolorum sed aliquam accusantium voluptatem non reiciendis provident voluptatibus culpa numquam non aut.", "calculate", 5 },
                    { 7, 24, new DateTime(2019, 2, 14, 6, 22, 30, 643, DateTimeKind.Unspecified).AddTicks(6604), new DateTime(2020, 10, 2, 7, 33, 38, 581, DateTimeKind.Unspecified).AddTicks(1646), "Mollitia sint possimus est blanditiis nobis quis at voluptas accusantium.", "Credit Card Account", 4 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 21, new DateTime(2017, 10, 15, 15, 43, 47, 88, DateTimeKind.Unspecified).AddTicks(6134), "Eos sunt est voluptatem iure ratione a dolorem sit sit.", new DateTime(2018, 8, 20, 21, 35, 11, 221, DateTimeKind.Unspecified).AddTicks(7689), "engage", 29, 3, 3 },
                    { 16, new DateTime(2018, 5, 28, 18, 6, 7, 770, DateTimeKind.Unspecified).AddTicks(7171), "Vel unde dolorum magni nostrum quod iure dolores nulla.", new DateTime(2017, 2, 22, 3, 10, 46, 230, DateTimeKind.Unspecified).AddTicks(3956), "connecting", 17, 3, 4 },
                    { 46, new DateTime(2018, 2, 1, 4, 56, 55, 702, DateTimeKind.Unspecified).AddTicks(9324), "Iste voluptatem excepturi voluptas autem ut sed.", new DateTime(2017, 6, 21, 19, 4, 42, 295, DateTimeKind.Unspecified).AddTicks(21), "Kuwait", 9, 1, 1 },
                    { 45, new DateTime(2017, 6, 12, 3, 36, 25, 845, DateTimeKind.Unspecified).AddTicks(3215), "Laudantium labore nobis ea nemo magni quidem.", new DateTime(2017, 11, 20, 18, 27, 54, 309, DateTimeKind.Unspecified).AddTicks(2023), "Manager", 16, 1, 1 },
                    { 35, new DateTime(2018, 10, 1, 18, 50, 45, 653, DateTimeKind.Unspecified).AddTicks(9202), "Ratione autem laboriosam qui et ipsum dolor ut quo et est veritatis aliquid.", new DateTime(2019, 9, 19, 17, 18, 38, 905, DateTimeKind.Unspecified).AddTicks(6174), "West Virginia", 28, 1, 3 },
                    { 33, new DateTime(2017, 12, 18, 13, 30, 41, 261, DateTimeKind.Unspecified).AddTicks(1547), "Voluptatem facere distinctio non fugit et modi in reiciendis dicta sequi.", new DateTime(2017, 5, 3, 8, 47, 26, 559, DateTimeKind.Unspecified).AddTicks(6115), "Savings Account", 15, 1, 4 },
                    { 26, new DateTime(2017, 2, 5, 13, 28, 58, 686, DateTimeKind.Unspecified).AddTicks(8569), "Aut magni nam ut molestiae voluptatibus ipsam odit illum.", new DateTime(2018, 4, 13, 23, 44, 59, 541, DateTimeKind.Unspecified).AddTicks(4254), "override", 27, 1, 3 },
                    { 17, new DateTime(2018, 10, 8, 8, 46, 37, 401, DateTimeKind.Unspecified).AddTicks(5517), "Omnis unde dicta nostrum ratione quisquam rerum deleniti fugit in aut ducimus.", new DateTime(2017, 7, 29, 14, 19, 59, 106, DateTimeKind.Unspecified).AddTicks(3583), "moderator", 11, 1, 1 },
                    { 9, new DateTime(2017, 1, 7, 4, 55, 17, 649, DateTimeKind.Unspecified).AddTicks(5515), "Expedita est sunt nemo est illum modi velit adipisci.", new DateTime(2018, 5, 25, 8, 45, 30, 749, DateTimeKind.Unspecified).AddTicks(4494), "vortals", 25, 1, 2 },
                    { 31, new DateTime(2017, 6, 16, 18, 22, 45, 301, DateTimeKind.Unspecified).AddTicks(8991), "Ut tenetur error itaque illo possimus.", new DateTime(2019, 6, 2, 17, 59, 5, 759, DateTimeKind.Unspecified).AddTicks(4607), "Toys, Movies & Jewelery", 16, 4, 3 },
                    { 6, new DateTime(2018, 4, 15, 20, 30, 25, 717, DateTimeKind.Unspecified).AddTicks(7652), "Asperiores excepturi perferendis ratione aut voluptatem occaecati aut dolores.", new DateTime(2020, 4, 17, 4, 29, 10, 397, DateTimeKind.Unspecified).AddTicks(562), "hacking", 27, 4, 4 },
                    { 38, new DateTime(2017, 12, 4, 13, 48, 3, 587, DateTimeKind.Unspecified).AddTicks(7486), "Temporibus enim voluptate ea aut doloremque a molestiae et architecto iusto libero voluptas.", new DateTime(2017, 4, 17, 22, 1, 27, 139, DateTimeKind.Unspecified).AddTicks(4657), "Way", 30, 3, 3 },
                    { 43, new DateTime(2017, 6, 28, 9, 41, 22, 844, DateTimeKind.Unspecified).AddTicks(1633), "Ipsum in voluptas necessitatibus vel ipsam accusamus dolore occaecati dolores dolorem saepe vel ut.", new DateTime(2018, 9, 13, 11, 30, 14, 975, DateTimeKind.Unspecified).AddTicks(3345), "Crossroad", 10, 5, 3 },
                    { 28, new DateTime(2017, 3, 3, 17, 26, 27, 764, DateTimeKind.Unspecified).AddTicks(5949), "Molestiae iste quis quasi omnis natus debitis sed saepe assumenda.", new DateTime(2017, 12, 28, 0, 28, 20, 355, DateTimeKind.Unspecified).AddTicks(1768), "Auto Loan Account", 25, 5, 2 },
                    { 27, new DateTime(2017, 8, 10, 8, 0, 35, 157, DateTimeKind.Unspecified).AddTicks(7521), "Eos dolor in sint est ut.", new DateTime(2020, 2, 27, 15, 27, 6, 756, DateTimeKind.Unspecified).AddTicks(7521), "plum", 18, 5, 2 },
                    { 24, new DateTime(2018, 4, 27, 4, 37, 57, 747, DateTimeKind.Unspecified).AddTicks(5271), "Iusto debitis consequatur explicabo in impedit eos dolores error.", new DateTime(2019, 10, 6, 21, 13, 38, 227, DateTimeKind.Unspecified).AddTicks(5638), "French Polynesia", 27, 5, 3 },
                    { 22, new DateTime(2018, 8, 27, 23, 20, 1, 370, DateTimeKind.Unspecified).AddTicks(5630), "Nihil illo sunt sit perspiciatis architecto.", new DateTime(2017, 6, 29, 23, 13, 2, 155, DateTimeKind.Unspecified).AddTicks(4734), "Jewelery & Movies", 21, 5, 2 },
                    { 4, new DateTime(2017, 3, 26, 13, 6, 10, 286, DateTimeKind.Unspecified).AddTicks(9963), "Id ut dignissimos nam tenetur laudantium quis molestiae voluptas est rerum.", new DateTime(2018, 1, 23, 19, 51, 0, 223, DateTimeKind.Unspecified).AddTicks(4696), "copy", 4, 5, 2 },
                    { 3, new DateTime(2017, 1, 3, 9, 29, 23, 663, DateTimeKind.Unspecified).AddTicks(2721), "Nostrum incidunt eaque qui commodi commodi sit error.", new DateTime(2017, 8, 19, 2, 30, 19, 135, DateTimeKind.Unspecified).AddTicks(742), "Wisconsin", 20, 5, 1 },
                    { 2, new DateTime(2017, 11, 2, 7, 44, 46, 958, DateTimeKind.Unspecified).AddTicks(9921), "Sit impedit eveniet unde qui voluptatem et et.", new DateTime(2019, 1, 28, 4, 4, 49, 292, DateTimeKind.Unspecified).AddTicks(9254), "Electronics & Outdoors", 24, 5, 1 },
                    { 12, new DateTime(2017, 12, 17, 19, 27, 26, 176, DateTimeKind.Unspecified).AddTicks(9281), "Cupiditate rerum aut ut omnis suscipit nihil ratione magni qui ut.", new DateTime(2017, 5, 11, 14, 1, 10, 883, DateTimeKind.Unspecified).AddTicks(3463), "Rwanda", 24, 4, 4 },
                    { 44, new DateTime(2018, 11, 25, 5, 58, 32, 427, DateTimeKind.Unspecified).AddTicks(2402), "Autem tempora non sit dolore occaecati ut libero temporibus.", new DateTime(2018, 9, 20, 17, 14, 49, 99, DateTimeKind.Unspecified).AddTicks(7653), "Borders", 30, 10, 4 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 11, new DateTime(2018, 5, 10, 11, 42, 51, 76, DateTimeKind.Unspecified).AddTicks(5320), "Quia qui blanditiis sit exercitationem qui maxime nobis perferendis saepe.", new DateTime(2020, 3, 26, 11, 35, 28, 64, DateTimeKind.Unspecified).AddTicks(480), "core", 4, 9, 2 },
                    { 5, new DateTime(2017, 3, 26, 0, 49, 40, 589, DateTimeKind.Unspecified).AddTicks(4270), "Eveniet inventore a at quia.", new DateTime(2019, 7, 1, 16, 33, 40, 833, DateTimeKind.Unspecified).AddTicks(9206), "Berkshire", 14, 7, 4 },
                    { 50, new DateTime(2018, 1, 9, 8, 53, 47, 328, DateTimeKind.Unspecified).AddTicks(1679), "Velit placeat architecto asperiores eaque id.", new DateTime(2018, 7, 4, 4, 26, 48, 290, DateTimeKind.Unspecified).AddTicks(2250), "Clothing", 23, 2, 2 },
                    { 30, new DateTime(2018, 3, 31, 8, 50, 41, 559, DateTimeKind.Unspecified).AddTicks(4309), "Qui saepe dolores maiores odit inventore doloremque non dolor dolore voluptate enim rerum ut.", new DateTime(2019, 5, 24, 9, 45, 48, 600, DateTimeKind.Unspecified).AddTicks(5207), "calculate", 3, 2, 4 },
                    { 23, new DateTime(2017, 4, 2, 1, 35, 14, 939, DateTimeKind.Unspecified).AddTicks(1514), "Tempore dolores qui mollitia veniam quibusdam.", new DateTime(2018, 6, 23, 11, 51, 52, 558, DateTimeKind.Unspecified).AddTicks(4056), "calculate", 20, 2, 4 },
                    { 19, new DateTime(2017, 1, 12, 11, 48, 24, 519, DateTimeKind.Unspecified).AddTicks(9458), "Voluptatibus praesentium delectus placeat expedita distinctio eos.", new DateTime(2019, 7, 24, 23, 52, 11, 710, DateTimeKind.Unspecified).AddTicks(8491), "Supervisor", 6, 2, 2 },
                    { 18, new DateTime(2017, 1, 5, 7, 10, 49, 161, DateTimeKind.Unspecified).AddTicks(320), "Dolorum delectus ut eveniet quia in dolore velit reiciendis.", new DateTime(2019, 8, 16, 14, 36, 41, 589, DateTimeKind.Unspecified).AddTicks(2245), "Colorado", 7, 2, 4 },
                    { 15, new DateTime(2017, 7, 10, 9, 18, 40, 146, DateTimeKind.Unspecified).AddTicks(9071), "Deleniti consectetur repudiandae temporibus eum quae cumque quae voluptatem et eum.", new DateTime(2017, 12, 21, 4, 36, 54, 740, DateTimeKind.Unspecified).AddTicks(3981), "Avon", 6, 2, 3 },
                    { 10, new DateTime(2018, 10, 24, 6, 25, 38, 215, DateTimeKind.Unspecified).AddTicks(8074), "Mollitia eos aut omnis ea omnis nihil sit ut tempora tempore sunt ut.", new DateTime(2018, 11, 4, 15, 16, 56, 291, DateTimeKind.Unspecified).AddTicks(8815), "magnetic", 23, 2, 4 },
                    { 1, new DateTime(2017, 8, 5, 19, 51, 20, 867, DateTimeKind.Unspecified).AddTicks(9198), "Voluptatum molestiae sunt aliquam omnis est quasi perferendis molestiae.", new DateTime(2018, 4, 2, 19, 39, 55, 519, DateTimeKind.Unspecified).AddTicks(1958), "International", 5, 2, 2 },
                    { 47, new DateTime(2017, 9, 9, 4, 56, 59, 301, DateTimeKind.Unspecified).AddTicks(1829), "Nobis ut animi iusto dolore reprehenderit sit.", new DateTime(2018, 1, 28, 18, 27, 26, 618, DateTimeKind.Unspecified).AddTicks(6941), "virtual", 18, 8, 1 },
                    { 39, new DateTime(2017, 9, 8, 15, 54, 19, 290, DateTimeKind.Unspecified).AddTicks(3344), "Veniam numquam accusantium non doloremque ut qui accusantium aut commodi dignissimos ea.", new DateTime(2018, 1, 22, 8, 35, 0, 885, DateTimeKind.Unspecified).AddTicks(9736), "Unbranded Plastic Keyboard", 11, 8, 3 },
                    { 34, new DateTime(2018, 6, 11, 3, 44, 32, 510, DateTimeKind.Unspecified).AddTicks(6362), "Ut sit qui occaecati nesciunt quos ex aliquid nobis esse hic rerum.", new DateTime(2018, 9, 16, 6, 53, 16, 85, DateTimeKind.Unspecified).AddTicks(1097), "Avon", 1, 8, 2 },
                    { 7, new DateTime(2017, 8, 19, 15, 52, 57, 967, DateTimeKind.Unspecified).AddTicks(982), "Aperiam quasi perspiciatis porro fugit nihil quia commodi voluptas facere voluptates.", new DateTime(2020, 6, 16, 8, 59, 8, 182, DateTimeKind.Unspecified).AddTicks(373), "complexity", 21, 8, 1 },
                    { 48, new DateTime(2017, 12, 7, 17, 18, 18, 107, DateTimeKind.Unspecified).AddTicks(9064), "Vitae porro quae aliquam qui eligendi quam neque.", new DateTime(2018, 4, 26, 9, 8, 1, 867, DateTimeKind.Unspecified).AddTicks(446), "Luxembourg", 20, 6, 4 },
                    { 42, new DateTime(2018, 1, 15, 1, 46, 35, 220, DateTimeKind.Unspecified).AddTicks(8772), "Aliquid et alias ad omnis et est qui eos laboriosam odio.", new DateTime(2018, 12, 31, 5, 1, 1, 331, DateTimeKind.Unspecified).AddTicks(8949), "e-business", 22, 6, 2 },
                    { 40, new DateTime(2017, 7, 3, 5, 8, 8, 795, DateTimeKind.Unspecified).AddTicks(2786), "Ea delectus minima earum rem debitis.", new DateTime(2020, 5, 30, 18, 53, 12, 382, DateTimeKind.Unspecified).AddTicks(8464), "Checking Account", 19, 6, 4 },
                    { 37, new DateTime(2017, 1, 25, 19, 48, 40, 76, DateTimeKind.Unspecified).AddTicks(7581), "Saepe quis culpa aliquid vel qui facilis minus.", new DateTime(2018, 4, 6, 7, 54, 11, 972, DateTimeKind.Unspecified).AddTicks(7340), "Alaska", 14, 6, 3 },
                    { 25, new DateTime(2017, 2, 4, 3, 55, 31, 992, DateTimeKind.Unspecified).AddTicks(2334), "Velit nesciunt optio et deserunt repellendus non eligendi numquam temporibus debitis suscipit.", new DateTime(2017, 9, 4, 10, 26, 41, 831, DateTimeKind.Unspecified).AddTicks(6358), "Configurable", 6, 6, 1 },
                    { 14, new DateTime(2017, 2, 28, 16, 37, 54, 468, DateTimeKind.Unspecified).AddTicks(4260), "Dolores sed ea porro dolor aut.", new DateTime(2019, 5, 7, 19, 21, 2, 592, DateTimeKind.Unspecified).AddTicks(2629), "Sleek", 24, 6, 3 },
                    { 8, new DateTime(2018, 4, 15, 1, 50, 23, 61, DateTimeKind.Unspecified).AddTicks(1221), "Omnis consequuntur odit voluptatem occaecati eos itaque eligendi ea ut eos expedita.", new DateTime(2019, 4, 22, 6, 29, 40, 534, DateTimeKind.Unspecified).AddTicks(8010), "Incredible", 15, 6, 1 },
                    { 49, new DateTime(2017, 5, 23, 15, 52, 49, 318, DateTimeKind.Unspecified).AddTicks(4225), "Qui in voluptatum ut ut molestiae aperiam aperiam est beatae.", new DateTime(2017, 1, 25, 18, 16, 15, 396, DateTimeKind.Unspecified).AddTicks(9840), "Handcrafted Frozen Salad", 4, 9, 1 },
                    { 41, new DateTime(2017, 3, 28, 21, 53, 44, 297, DateTimeKind.Unspecified).AddTicks(4142), "Fugiat quia doloribus consectetur autem dignissimos alias est rem officia sapiente architecto tempora ea.", new DateTime(2019, 10, 25, 13, 33, 6, 240, DateTimeKind.Unspecified).AddTicks(3781), "open architecture", 21, 9, 4 },
                    { 32, new DateTime(2017, 3, 5, 21, 46, 2, 583, DateTimeKind.Unspecified).AddTicks(2014), "Accusamus quas quia quo sed.", new DateTime(2018, 4, 21, 23, 23, 59, 696, DateTimeKind.Unspecified).AddTicks(1748), "Regional", 29, 9, 1 },
                    { 29, new DateTime(2018, 3, 20, 17, 58, 1, 813, DateTimeKind.Unspecified).AddTicks(7068), "Ut quod facilis accusamus laborum eaque rem sed maiores.", new DateTime(2019, 12, 9, 14, 5, 29, 428, DateTimeKind.Unspecified).AddTicks(4462), "Sudanese Pound", 20, 9, 3 },
                    { 13, new DateTime(2018, 6, 8, 20, 57, 38, 61, DateTimeKind.Unspecified).AddTicks(4367), "Est nobis unde aliquam cum doloremque.", new DateTime(2017, 1, 5, 21, 56, 42, 774, DateTimeKind.Unspecified).AddTicks(219), "Incredible", 26, 9, 3 },
                    { 20, new DateTime(2018, 10, 5, 11, 10, 13, 465, DateTimeKind.Unspecified).AddTicks(9060), "Recusandae inventore sint natus rem consequuntur quia est repudiandae saepe eveniet occaecati.", new DateTime(2018, 9, 11, 16, 35, 38, 908, DateTimeKind.Unspecified).AddTicks(3491), "reboot", 7, 7, 1 },
                    { 36, new DateTime(2018, 11, 6, 4, 17, 57, 24, DateTimeKind.Unspecified).AddTicks(1660), "Ipsum quia voluptas eum aut fuga explicabo ut cum.", new DateTime(2017, 7, 5, 10, 55, 6, 911, DateTimeKind.Unspecified).AddTicks(2861), "Adaptive", 5, 7, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_StateId",
                table: "Tasks",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "TaskStates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
