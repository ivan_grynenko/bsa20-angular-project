﻿using System;

namespace API.Models
{
    public class UserInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int? OverallNumOfTasks { get; set; }
        public int? OverallNumOfCanceledTasks { get; set; }
        public TasksDTO LongestTask { get; set; }
    }
}
