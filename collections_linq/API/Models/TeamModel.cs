﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class TeamModel
    {
        [Required]
        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
    }
}
