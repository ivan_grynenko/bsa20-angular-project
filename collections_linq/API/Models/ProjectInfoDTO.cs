﻿using DAL.Models;
using System;

namespace API.Models
{
    public class ProjectInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TasksDTO LongestTask { get; set; }
        public TasksDTO ShortestTask { get; set; }
    }
}
