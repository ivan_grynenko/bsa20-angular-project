﻿using API.Models;
using AutoMapper;
using BL.Models;
using DAL.Models;
using System.Security.Cryptography;

namespace API.Helpers
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<User, UserDTO>()
                .ReverseMap();
            CreateMap<Project, ProjectDTO>()
                .ReverseMap();
            CreateMap<Project, ProjectModel>()
                .ReverseMap();
            CreateMap<Team, TeamDTO>()
                .ReverseMap();
            CreateMap<TasksDTO, Tasks>()
                .ForMember(dest => dest.State, opt => opt.Ignore())
                .ForMember(dest => dest.Project, opt => opt.Ignore())
                .ForMember(dest => dest.Performer, opt => opt.Ignore())
                //.ForMember(dest => dest.Id, opt => opt.nu)
                //.ForMember(dest => dest.State, opt => opt.MapFrom(src => src.StateId))
                .ReverseMap();
                //.ForPath(s => s.StateId, opt => opt.MapFrom(src => src.State));
            CreateMap<ProjectInfo, ProjectInfoDTO>();
            CreateMap<UserInfo, UserInfoDTO>();
            CreateMap<UserTasks, UserTasksDTO>();
        }
    }
}
