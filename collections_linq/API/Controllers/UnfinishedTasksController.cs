﻿using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnfinishedTasksController : ControllerBase
    {
        private readonly ITaskService taskService;
        private readonly IMapper mapper;
        private readonly ILoggerService loggerService;

        public UnfinishedTasksController(ITaskService taskService, IMapper mapper, ILoggerService loggerService)
        {
            this.taskService = taskService;
            this.mapper = mapper;
            this.loggerService = loggerService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TasksDTO>>> GetUnfinishedTasks(int userId)
        {
            if (userId < 1)
                return BadRequest();

            var user = await taskService.GetById(userId);

            if (user == null)
                return NotFound();

            var result = await taskService.GetUnfinishedTasksByUser(userId);

            return Ok(mapper.Map<IEnumerable<TasksDTO>>(result));
        }
    }
}
