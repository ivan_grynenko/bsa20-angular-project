﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly ILoggerService loggerService;

        public UsersController(IUserService userService, IMapper mapper, ILoggerService loggerService)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.loggerService = loggerService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            return Ok(mapper.Map<IEnumerable<UserDTO>>(await userService.GetAll()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUserById(int id)
        {
            if (id < 1)
                return BadRequest();

            var user = await userService.GetById(id);

            if (user == null)
                return NotFound();
            else
                return Ok(mapper.Map<UserDTO>(user));
        }

        [HttpGet("OrderWithTasks")]
        public async Task<ActionResult<IEnumerable<UserTasksDTO>>> GetOrderedUsersWithOrderedTasks()
        {
            return Ok(mapper.Map<IEnumerable<UserTasksDTO>>(await userService.GetOrderedUsersWithOrderedTasks()));
        }

        [HttpGet("{id}/UserInfo")]
        public async Task<ActionResult<UserInfoDTO>> GetUserInfo(int id)
        {
            if (id < 1)
                return BadRequest();

            var user = await userService.GetById(id);

            if (user == null)
                return NotFound();
            var r = mapper.Map<UserInfoDTO>(await userService.GetUserInfo(id));
            return Ok(r);
        }

        [HttpPost("multiple")]
        public async Task<IActionResult> AddUsers([FromBody] IEnumerable<ProjectDTO> newUsers)
        {
            if (newUsers.Any(e => !TryValidateModel(e)))
                return BadRequest();

            await userService.Add(mapper.Map<IEnumerable<User>>(newUsers));

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody] ProjectDTO newUser)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await userService.Add(mapper.Map<User>(newUser));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = await userService.GetById(id);

            if (item == null)
                return NotFound();

            await userService.Remove(item);

            return NoContent();
        }

        [HttpPatch("multiple")]
        public async Task<IActionResult> UpdateUsers([FromBody] IEnumerable<UserDTO> newUsers)
        {
            await userService.Update(mapper.Map<IEnumerable<User>>(newUsers));

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateUser([FromBody] UserDTO newUser)
        {
            await userService.Update(mapper.Map<User>(newUser));

            return NoContent();
        }
    }
}
