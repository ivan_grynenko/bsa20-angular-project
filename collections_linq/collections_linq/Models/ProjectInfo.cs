﻿using System;

namespace collections_linq.Models
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Tasks LongestTask { get; set; }
        public Tasks ShortestTask { get; set; }
    }
}
