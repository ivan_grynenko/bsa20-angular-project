﻿using collections_linq.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;

namespace collections_linq
{
    class DataWorker : IDisposable
    {
        private HttpClient client;
        private bool isDisposed = false;

        public DataWorker()
        {
            client = new HttpClient();            
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MainUri"]);
        }

        public async Task<IEnumerable<TEntity>> GetMultipleResponse<TEntity>(string uri)
            where TEntity : class
        {
            IEnumerable<TEntity> items = null;
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
                items = await response.Content.ReadAsJsonAsync<IEnumerable<TEntity>>();

            return items;
        }

        public async Task<TEntity> GetResponse<TEntity>(string uri)
            where TEntity : class
        {
            TEntity item = null;
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
                item = await response.Content.ReadAsJsonAsync<TEntity>();

            return item;
        }

        public async Task<HttpResponseMessage> Patch<TEntity>(string uri, TEntity obj)
            where TEntity : class
        {
            var content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
            var patchResponse = await client.PatchAsync(uri, content);

            return patchResponse;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
                client.Dispose();

            isDisposed = true;
        }
    }
}
