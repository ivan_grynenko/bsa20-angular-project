﻿using BL.Models;
using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IProjectService : IService<Project>
    {
        Task<IEnumerable<ProjectInfo>> GetProjectWithTasks();
    }
}