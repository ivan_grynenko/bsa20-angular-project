﻿using API.Models;
using Bogus.DataSets;
using DAL;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace API.IntegrationTests
{
    public class TeamControllerIntegrationTest : IClassFixture<CustomeWebAppFactory<Startup>>
    {
        private readonly HttpClient client;

        public TeamControllerIntegrationTest(CustomeWebAppFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }

        [Fact]
        public async void AddTeam_ValidModel_SuccessCode()
        {
            var newTeam = new TeamDTO()
            {
                Name = "Team BSA",
                CreatedAt = DateTime.Now
            };
            var teamJson = JsonConvert.SerializeObject(newTeam);
            var httpResponse = await client.PostAsync("api/teams", new StringContent(teamJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var opt = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("InMemoryTestDB")
                .Options;

            using (var context = new ApplicationContext(opt))
            {
                Assert.Equal(6, context.Teams.Count());
            }
        }

        [Fact]
        public async void AddTeam_InvalidModel_BadRequest()
        {
            var newTeam = new TeamDTO()
            {
                CreatedAt = DateTime.Now
            };
            var teamJson = JsonConvert.SerializeObject(newTeam);
            var httpResponse = await client.PostAsync("api/teams", new StringContent(teamJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Fact]
        public async void GetTeamsOfCertainAge_YoungerThan60_NotEmpty()
        {
            var age = 5;
            var httpResponse = await client.GetAsync($"api/teams/minage/{age}");
            var resultString = await httpResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject(resultString);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.NotEmpty((dynamic)result);
        }

        [Fact]
        public async void GetTeamsOfCertainAgeOlderThan60_Empty()
        {
            var age = 80;
            var httpResponse = await client.GetAsync($"api/teams/minage/{age}");
            var resultString = await httpResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject(resultString);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Empty((dynamic)result);
        }
    }
}
