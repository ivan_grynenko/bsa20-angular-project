﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace API.IntegrationTests
{
    public class UnfinishedTasksControllerIntegrationTest : IClassFixture<CustomeWebAppFactory<Startup>>
    {
        private readonly HttpClient client;

        public UnfinishedTasksControllerIntegrationTest(CustomeWebAppFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }

        [Theory]
        [InlineData(1, HttpStatusCode.OK)]
        [InlineData(61, HttpStatusCode.NotFound)]
        [InlineData(-1, HttpStatusCode.BadRequest)]
        public async void GetUnfinishedTasks(int userId, HttpStatusCode expectedCode)
        {
            var httpResponse = await client.GetAsync($"api/unfinishedtasks?userId={userId}");

            Assert.Equal(expectedCode, httpResponse.StatusCode);
        }
    }
}
