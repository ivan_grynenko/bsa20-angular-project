using API.Models;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using Xunit;

namespace API.IntegrationTests
{
    public class ProjectsControllerIntegrationTest : IClassFixture<CustomeWebAppFactory<Startup>>
    {
        private readonly HttpClient client;

        public ProjectsControllerIntegrationTest(CustomeWebAppFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }

        [Fact]
        public async void Add_ValidModel_SuccessCode()
        {
            var newProject = new ProjectDTO()
            {
                AuthorId = 1,
                CreatedAt = new DateTime(2020, 1, 1),
                Deadline = new DateTime(2020, 10, 1),
                Description = "some text",
                Name = "Project 1",
                TeamId = 1
            };
            string projectJson = JsonConvert.SerializeObject(newProject);
            var httpResponse = await client.PostAsync(@"api/projects", new StringContent(projectJson, Encoding.UTF8, "application/json"));
            httpResponse.EnsureSuccessStatusCode();

            Assert.Equal(System.Net.HttpStatusCode.NoContent, httpResponse.StatusCode);

            var opt = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("InMemoryTestDB")
                .Options;

            using (var context = new ApplicationContext(opt))
            {
                Assert.Equal(11, context.Projects.Count());
            }
        }

        [Fact]
        public async void Add_MissingData_BadRequest()
        {
            var newProject = new ProjectDTO()
            {
                AuthorId = 1,
                CreatedAt = new DateTime(2020, 1, 1),
                Deadline = new DateTime(2020, 10, 1),
                Description = "some text",
                TeamId = 1
            };
            string projectJson = JsonConvert.SerializeObject(newProject);
            var httpResponse = await client.PostAsync(@"api/projects", new StringContent(projectJson, Encoding.UTF8, "application/json"));

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
