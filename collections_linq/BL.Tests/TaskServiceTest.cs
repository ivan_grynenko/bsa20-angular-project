﻿using BL.Interfaces;
using BL.Services;
using DAL.Base;
using DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace BL.Tests
{
    public class TaskServiceTest
    {
        private ITaskService taskService;
        private IUnitOfWork unitOFWork;

        public TaskServiceTest()
        {
            unitOFWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOFWork.ProjectRepository.GetAll()).Returns(new List<Project>()
            {
                new Project()
                {
                    Id = 1,
                    AuthorId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    Deadline = new DateTime(2025, 1, 1),
                    Description = "some description",
                    Name = "Project 1",
                    TeamId = 1
                }
            });
            A.CallTo(() => unitOFWork.TaskRepository.GetAll()).Returns(new List<Tasks>()
            {
                new Tasks()
                {
                    Id = 1,
                    Name = "Task 1",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 3, 1),
                    Description = new string('a', 10),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 2,
                    Name = "Task 2",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 7, 1),
                    Description = new string('a', 1),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 3,
                    Name = "Task",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 5, 1),
                    Description = new string('a', 3),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 4,
                    Name = "Task 4",
                    ProjectId = 2,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 4, 1),
                    Description = new string('a', 7),
                    PerformerId = 2
                },
                new Tasks()
                {
                    Id = 5,
                    Name = "Task 5",
                    ProjectId = 2,
                    CreatedAt = new DateTime(2018, 3, 1),
                    FinishedAt = new DateTime(2019, 4, 1),
                    Description = new string('a', 7),
                    PerformerId = 3
                }
            });
            A.CallTo(() => unitOFWork.UserRepository.GetAll()).Returns(new List<User>()
            {
                new User()
                {
                    Id = 1,
                    FirstName = "Rick",
                    LastName = "Sanchez",
                    Email = "email@",
                    TeamId = 1
                }
            });

            taskService = new TaskService(unitOFWork);
        }

        [Fact]
        public async void GetNumberOfTasksByUser_ExistingId_ValidData()
        {
            var result = await taskService.GetNumberOfTasksByUser(1);

            Assert.NotEmpty(result);
            Assert.Equal(3, result.ElementAt(0).Value);
        }

        [Fact]
        public async void GetNumberOfTasksByUser_NotExistingId_Empty()
        {
            var result = await taskService.GetNumberOfTasksByUser(2);

            Assert.Empty(result);
        }

        [Theory]
        [InlineData(1, 5, 1)]
        [InlineData(2, 5, 0)]
        [InlineData(1, 3, 0)]
        public async void GetTasksByUserWithMaxNameLength(int userId, int maxLength, int expectedResult)
        {
            var result = await taskService.GetTasksByUserWithMaxNameLength(userId, maxLength);

            Assert.Equal(expectedResult, result.Count());
        }

        [Theory]
        [InlineData(1, 3)]
        [InlineData(3, 0)]
        public async void GetTasksByUserInCurrentYear(int userId, int expectedResult)
        {
            var result = await taskService.GetTasksByUserInCurrentYear(userId);

            Assert.Equal(expectedResult, result.Count());
        }

        [Fact]
        public async void Update()
        {
            await taskService.Update(new Tasks());

            A.CallTo(() => unitOFWork.TaskRepository.Update(A<Tasks>.That.IsInstanceOf(typeof(Tasks)))).MustHaveHappened();
            A.CallTo(() => unitOFWork.SaveChanges()).MustHaveHappened();
        }

        [Fact]
        public async void GetUnfinishedTasksByUser_ValidUserId_NotEmpty()
        {
            var result = await taskService.GetUnfinishedTasksByUser(1);

            Assert.NotEmpty(result);
            Assert.Empty(result.Where(e => e.StateId == 3));
        }

        [Fact]
        public async void GetUnfinishedTasksByUser_InvalidUserId_Empty()
        {
            var result = await taskService.GetUnfinishedTasksByUser(31);

            Assert.Empty(result);
        }
    }
}
