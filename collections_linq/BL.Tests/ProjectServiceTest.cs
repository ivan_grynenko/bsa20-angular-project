using BL.Interfaces;
using BL.Services;
using Xunit;
using FakeItEasy;
using DAL.Base;
using System.Collections.Generic;
using DAL.Models;
using System;
using System.Linq;

namespace BL.Tests
{
    public class ProjectServiceTest
    {
        private IProjectService projectService;
        private IUnitOfWork unitOFWork;

        public ProjectServiceTest()
        {
            unitOFWork = A.Fake<IUnitOfWork>(); 
        }

        [Fact]
        public async void GetProjectWithTasks_ValidData_RetuensSomeData()
        {
            A.CallTo(() => unitOFWork.ProjectRepository.GetAll()).Returns(new List<Project>()
            {
                new Project()
                {
                    Id = 1,
                    AuthorId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    Deadline = new DateTime(2025, 1, 1),
                    Description = "some description",
                    Name = "Project 1",
                    TeamId = 1
                }
            });
            A.CallTo(() => unitOFWork.UserRepository.GetAll()).Returns(new List<User>()
            {
                new User()
                {
                    Id = 1,
                    FirstName = "Rick",
                    LastName = "Sanchez",
                    Email = "email@",
                    TeamId = 1
                }
            });
            A.CallTo(() => unitOFWork.TaskRepository.GetAll()).Returns(new List<Tasks>()
            {
                new Tasks()
                {
                    Id = 1,
                    Name = "Task 1",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 3, 1),
                    Description = new string('a', 10),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 2,
                    Name = "Task 2",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 7, 1),
                    Description = new string('a', 1),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 3,
                    Name = "Task",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 5, 1),
                    Description = new string('a', 3),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 4,
                    Name = "Task 4",
                    ProjectId = 2,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 4, 1),
                    Description = new string('a', 7),
                    PerformerId = 2
                }
            });

            projectService = new ProjectService(unitOFWork);

            var result = await projectService.GetProjectWithTasks();

            Assert.Single(result);
            Assert.Equal(1, result.ToList()[0].LongestTask.Id);
            Assert.Equal(3, result.ToList()[0].ShortestTask.Id);
        }

        [Fact]
        public async void GetProjectWithTasks_MismatchingId_RetuensNull()
        {
            A.CallTo(() => unitOFWork.ProjectRepository.GetAll()).Returns(new List<Project>()
            {
                new Project()
                {
                    Id = 3,
                    AuthorId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    Deadline = new DateTime(2025, 1, 1),
                    Description = "some description",
                    Name = "Project 1",
                    TeamId = 1
                }
            });
            A.CallTo(() => unitOFWork.UserRepository.GetAll()).Returns(new List<User>()
            {
                new User()
                {
                    Id = 1,
                    FirstName = "Rick",
                    LastName = "Sanchez",
                    Email = "email@",
                    TeamId = 1
                }
            });
            A.CallTo(() => unitOFWork.TaskRepository.GetAll()).Returns(new List<Tasks>()
            {
                new Tasks()
                {
                    Id = 1,
                    Name = "Task 1",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 3, 1),
                    Description = new string('a', 10),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 2,
                    Name = "Task 2",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 7, 1),
                    Description = new string('a', 1),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 3,
                    Name = "Task",
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 5, 1),
                    Description = new string('a', 3),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 4,
                    Name = "Task 4",
                    ProjectId = 2,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 4, 1),
                    Description = new string('a', 7),
                    PerformerId = 2
                }
            });

            projectService = new ProjectService(unitOFWork);

            var result = await projectService.GetProjectWithTasks();

            Assert.Empty(result);
        }
    }
}
