﻿using BL.Interfaces;
using BL.Services;
using DAL.Base;
using DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace BL.Tests
{
    public class UserServiceTest
    {
        private IUserService userService;
        private IUnitOfWork unitOFWork;

        public UserServiceTest()
        {
            unitOFWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOFWork.TaskRepository.GetAll()).Returns(new List<Tasks>()
            {
                new Tasks()
                {
                    Id = 1,
                    Name = "Task" + new string('*', 3),
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 3, 1),
                    Description = new string('a', 10),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 2,
                    Name = "Task" + new string('*', 10),
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 1, 1),
                    FinishedAt = new DateTime(2020, 7, 1),
                    Description = new string('a', 1),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 3,
                    Name = "Task" + new string('*', 1),
                    ProjectId = 1,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 5, 1),
                    Description = new string('a', 3),
                    PerformerId = 1
                },
                new Tasks()
                {
                    Id = 4,
                    Name = "Task" + new string('*', 1),
                    ProjectId = 2,
                    CreatedAt = new DateTime(2020, 3, 1),
                    FinishedAt = new DateTime(2020, 4, 1),
                    Description = new string('a', 7),
                    PerformerId = 2
                },
                new Tasks()
                {
                    Id = 5,
                    Name = "Task" + new string('*', 3),
                    ProjectId = 2,
                    CreatedAt = new DateTime(2018, 3, 1),
                    FinishedAt = new DateTime(2019, 4, 1),
                    Description = new string('a', 7),
                    PerformerId = 2
                }
            });
            A.CallTo(() => unitOFWork.UserRepository.GetAll()).Returns(new List<User>()
            {
                new User()
                {
                    Id = 1,
                    FirstName = "Rick",
                    LastName = "Sanchez",
                    Email = "email@",
                    TeamId = 1,
                    Birthday = new DateTime(1940, 1, 1)
                },
                new User()
                {
                    Id = 2,
                    FirstName = "Morty",
                    LastName = "Smith",
                    Email = "email2@",
                    TeamId = 2,
                    Birthday = new DateTime(2010, 1, 1)
                }
            });
            A.CallTo(() => unitOFWork.ProjectRepository.GetAll()).Returns(new List<Project>()
            {
                new Project()
                {
                    Id = 1,
                    AuthorId = 1,
                    CreatedAt = new DateTime(2019, 1, 1),
                    Deadline = new DateTime(2020, 12, 12),
                    Description = "",
                    Name = "Project A",
                    TeamId = 1
                },
                new Project()
                {
                    Id = 2,
                    AuthorId = 2,
                    CreatedAt = new DateTime(2020, 1, 1),
                    Deadline = new DateTime(2022, 1, 1),
                    Description = "",
                    Name = "Project B",
                    TeamId = 2
                }
            });
            userService = new UserService(unitOFWork);
        }

        [Theory]
        [InlineData(5, 0)]
        [InlineData(2, 1)]
        public async void GetOrderedUsersWithOrderedTasks(int expectedFirstTask, int elementIndex)
        {
            var result = await userService.GetOrderedUsersWithOrderedTasks();

            Assert.Equal(expectedFirstTask, result.ElementAt(elementIndex).Tasks.ElementAt(0).Id);
        }

        [Fact]
        public async void GetUserInfo_ValidUserId_NotNull()
        {
            var result = await userService.GetUserInfo(1);

            Assert.NotNull(result);
            Assert.Equal(1, result.User.Id);
        }

        [Fact]
        public async void GetUserInfo_InvalidUserId_Null()
        {
            var result = await userService.GetUserInfo(10);

            Assert.Null(result);
        }

        [Fact]
        public async void Add_MethodsAreCalled()
        {
            await userService.Add(new User());

            A.CallTo(() => unitOFWork.UserRepository.Add(A<User>.That.IsInstanceOf(typeof(User)))).MustHaveHappened();
            A.CallTo(() => unitOFWork.SaveChanges()).MustHaveHappened();
        }

        [Fact]
        public async void Update()
        {
            await userService.Update(new User());

            A.CallTo(() => unitOFWork.UserRepository.Update(A<User>.That.IsInstanceOf(typeof(User)))).MustHaveHappened();
            A.CallTo(() => unitOFWork.SaveChanges()).MustHaveHappened();
        }
    }
}
