import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsListComponent } from './projects/projects-list/projects-list.component';
import { TeamsListComponent } from './teams/teams-list/teams-list.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { TasksListComponent } from './tasks/tasks-list/tasks-list.component';

const routes: Routes = [
  { path: 'projects', component: ProjectsListComponent},
  { path: 'tasks', component: TasksListComponent},
  { path: 'teams', component: TeamsListComponent},
  { path: 'users', component: UsersListComponent},
  { path: '', redirectTo: '/projects', pathMatch: 'full'},
  // { path: '**', component: notfound}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
