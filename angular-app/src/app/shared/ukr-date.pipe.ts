import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ukrDate'
})
export class UkrDatePipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): string {
    let month;
    let date = new Date(value);
    switch (date.getMonth()) {
      case 0:
        month = 'січня';
        break;
      case 1:
        month = 'лютого';
        break;
      case 2:
        month = 'березеня';
        break;
      case 3:
        month = 'квітня';
        break;
      case 4:
        month = 'травня';
        break;
      case 5:
        month = 'червня';
        break;
      case 6:
        month = 'липня';
        break;
      case 7:
        month = 'серпня';
        break;
      case 8:
        month = 'вересня';
        break;
      case 9:
        month = 'жовтня';
        break;
      case 10:
        month = 'листопада';
        break;
      case 11:
        month = 'грудня';
        break;
      default:
        break;
    }

    return `${date.getDay()} ${month} ${date.getFullYear()}`;
  }

}
