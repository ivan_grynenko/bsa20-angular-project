import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UkrDatePipe } from './ukr-date.pipe';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    UkrDatePipe,
    DeleteDialogComponent
  ],
  exports: [
    UkrDatePipe
  ],
  imports: [
    CommonModule,
    MatDialogModule
  ]
})
export class SharedModule { }
