import { TaskState } from './TaskState';

export interface ITask {
    id: number,
    name: string,
    description: string,
    createdAt: Date,
    finishedAt: Date,
    stateId: TaskState,
    projectId: number
    performerId: number
}
