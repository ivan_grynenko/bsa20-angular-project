export interface IProject {
    id: number,
    name: string,
    description: string,
    createdAt: Date,
    deadline: Date,
    authorId: number,
    teamId: number
}
