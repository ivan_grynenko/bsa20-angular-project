export enum TaskState {
    Created = 1,
    Started,
    Finished,
    Canceled
}