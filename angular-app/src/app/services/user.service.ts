import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { IUser } from '../models/user.model';
import { WebService } from '../services/web.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private projecstUrl: string = '/users';

  constructor(
    private webService: WebService<IUser>
  ) { }

  getAllUsers(): Observable<IUser[]> {
    return this.webService.getAll<IUser>(this.projecstUrl);
  }

  deleteUser(id: number): Observable<Object> {
    return this.webService.deleteEntry(this.projecstUrl + `/${id}`);
  }

  updateUser(project: IUser) {
    return this.webService.updateEntry<IUser>(this.projecstUrl, project);
  }
}
