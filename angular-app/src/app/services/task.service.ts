import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ITask } from '../models/task.model';
import { WebService } from '../services/web.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private projecstUrl: string = '/tasks';

  constructor(
    private webService: WebService<ITask>
  ) { }

  getAllTasks(): Observable<ITask[]> {
    return this.webService.getAll<ITask>(this.projecstUrl);
  }

  deleteTask(id: number): Observable<Object> {
    return this.webService.deleteEntry(this.projecstUrl + `/${id}`);
  }

  updateTask(project: ITask) {
    return this.webService.updateEntry<ITask>(this.projecstUrl, project);
  }
}
