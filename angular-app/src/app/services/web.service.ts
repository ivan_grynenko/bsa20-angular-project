import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { API } from '../helpers/global-resoursec';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebService<T> {

  constructor(
    private http: HttpClient
  ) { }

  getAll<T>(url: string): Observable<T[]> {
    return this.http.get<T[]>(API + url);
  }

  deleteEntry(url: string): Observable<object> {
    return this.http.delete(API + url);
  }

  updateEntry<T>(url: string, entry: T) {
    return this.http.patch(API + url, entry);
  }

}
