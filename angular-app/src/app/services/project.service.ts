import { Injectable } from '@angular/core';

import { WebService } from './web.service';
import { IProject } from '../models/project.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private projecstUrl: string = '/projects';

  constructor(
    private webService: WebService<IProject>
  ) { }

  getAllProjects(): Observable<IProject[]> {
    return this.webService.getAll<IProject>(this.projecstUrl);
  }

  deleteProject(id: number): Observable<Object> {
    return this.webService.deleteEntry(this.projecstUrl + `/${id}`);
  }

  updateProject(project: IProject) {
    return this.webService.updateEntry<IProject>(this.projecstUrl, project);
  }
}
