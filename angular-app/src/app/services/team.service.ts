import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ITeam } from '../models/team.model';
import { WebService } from '../services/web.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private projecstUrl: string = '/teams';

  constructor(
    private webService: WebService<ITeam>
  ) { }

  getAllTeams(): Observable<ITeam[]> {
    return this.webService.getAll<ITeam>(this.projecstUrl);
  }

  deleteTeam(id: number): Observable<Object> {
    return this.webService.deleteEntry(this.projecstUrl + `/${id}`);
  }

  updateTeam(project: ITeam) {
    return this.webService.updateEntry<ITeam>(this.projecstUrl, project);
  }
}
