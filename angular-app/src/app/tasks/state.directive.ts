import { Directive, ElementRef, OnInit, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit {

  @Input() text: string;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) { }

   ngOnInit() {
    this.checkState();
   }

   checkState(): void {
    if (this.text === 'Started') {
      this.elementRef.nativeElement.innerText = this.text;
      this.renderer.setStyle(this.elementRef.nativeElement, 'color', '#0AEE0D');
    }   
    else if (this.text === 'Created') {
      this.elementRef.nativeElement.innerText = this.text;
      this.renderer.setStyle(this.elementRef.nativeElement, 'color', '#EFF60A');
    }
    else if (this.text === 'Finished') {
      this.elementRef.nativeElement.innerText = this.text;
      this.renderer.setStyle(this.elementRef.nativeElement, 'color', '#690FFB');
    }
    else if (this.text === 'Canceled') {
      this.elementRef.nativeElement.innerText = this.text;
      this.renderer.setStyle(this.elementRef.nativeElement, 'color', '#FB0F0F');
    }
   }

}
