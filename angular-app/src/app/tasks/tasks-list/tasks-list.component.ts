import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ITask } from '../../models/task.model';
import { TaskService } from '../../services/task.service';
import { TaskState } from '../../models/TaskState';
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

  currentTasks: ITask[];
  TaskState: typeof TaskState = TaskState;
  loading: boolean = false;

  constructor(
    private taskService: TaskService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.getAllTasks();
  }

  private getAllTasks(): void {
    this.taskService.getAllTasks().subscribe(tasks => {
      this.currentTasks = tasks;
      this.loading = false;
    },
    error => {
      console.log(error);
      this.loading = false;
    });
  }

  editTask(task: ITask) {

  }

  deleteTask(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this.taskService.deleteTask(id).subscribe(result => {
          //+ toastr success message
          this.getAllTasks();
          this.loading = false;
        })
        error => {
          console.log(error);
          this.loading = false;
        }
      }
    });
  }
}
