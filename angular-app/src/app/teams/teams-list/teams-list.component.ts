import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { TeamService } from '../../services/team.service';
import { ITeam } from '../../models/team.model';
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {

  currentTeams: ITeam[];
  loading: boolean = false;

  constructor(
    private teamService: TeamService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.getAllTeams();
  }

  private getAllTeams(): void {
    this.teamService.getAllTeams().subscribe(teams => {
      this.currentTeams = teams;
      this.loading = false;
    },
    error => {
      console.log(error);
      this.loading = false;
    });
  }

  editTeam(team: ITeam) {

  }

  deleteTeam(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this.teamService.deleteTeam(id).subscribe(result => {
          //+ toastr success message
          this.getAllTeams();
          this.loading = false;
        })
        error => {
          console.log(error);
          this.loading = false;
        }
      }
    });
  }

}
