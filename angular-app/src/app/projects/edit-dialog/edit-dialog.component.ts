import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IProject } from '../../models/project.model';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css']
})
export class EditDialogComponent implements OnInit {

  project: IProject;
  projectForm : FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IProject,
    private projectService: ProjectService
  ) { }

  ngOnInit(): void {
    this.project = this.data;
    this.projectForm = new FormGroup({
      "name": new FormControl("", Validators.required),
      "description": new FormControl(""),
      "created": new FormControl("", Validators.required),
      deadline: new FormControl("", Validators.required),
      "authorId": new FormControl("", [Validators.required, Validators.pattern(/^\d+$/)]),
      "teamId": new FormControl("", [Validators.required, Validators.pattern(/^\d+$/)]),
    });
    this.projectForm.controls['name'].setValue(this.project.name);
    this.projectForm.controls['description'].setValue(this.project.description);
    this.projectForm.controls['created'].setValue(
      new Date(this.project.createdAt)
    );
    this.projectForm.controls['deadline'].setValue(
      new Date(this.project.deadline)
    );
    this.projectForm.controls['authorId'].setValue(this.project.authorId);
    this.projectForm.controls['teamId'].setValue(this.project.teamId);
  }

  submit() {
    this.projectService.updateProject({
      id: this.project.id,
      authorId: this.projectForm.controls['authorId'].value,
      createdAt: this.projectForm.controls['created'].value,
      deadline: this.projectForm.controls['deadline'].value,
      description: this.projectForm.controls['description'].value,
      name: this.projectForm.controls['name'].value,
      teamId: this.projectForm.controls['teamId'].value
    }).subscribe(result => {
      //loading
      this.dialogRef.close();
    },
    error => {
      console.log(error);
    });
  }

  formIsValid() {
    return this.projectForm.valid;
  }

}
