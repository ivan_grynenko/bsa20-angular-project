import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ProjectService } from '../../services/project.service';
import { IProject } from '../../models/project.model';
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

  currentProjects: IProject[];
  loading: boolean = false;

  constructor(
    private projectService: ProjectService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.getAllProjects();
  }

  private getAllProjects(): void {
    this.projectService.getAllProjects().subscribe(projects => {
      this.currentProjects = projects;
      this.loading = false;
    },
    error => {
      console.log(error);
      this.loading = false;
    });
  }

  deleteProject(num: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this.projectService.deleteProject(num).subscribe(result => {
          //+ toastr success message
          this.getAllProjects();
          this.loading = false;
        })
        error => {
          console.log(error);
          this.loading = false;
        }
      }
    });
  }

  editProject(project: IProject) {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      minWidth: '350px',
      minHeight: '450px',
      data: project
    });
  }

}
