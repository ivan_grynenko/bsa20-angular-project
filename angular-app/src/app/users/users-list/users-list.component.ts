import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { UserService } from '../../services/user.service';
import { IUser } from '../../models/user.model';
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  currentUsers: IUser[];
  loading: boolean = false;

  constructor(
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.getAllUsers();
  }

  private getAllUsers(): void {
    this.userService.getAllUsers().subscribe(users => {
      this.currentUsers = users;
      this.loading = false;
    },
    error => {
      console.log(error);
      this.loading = false;
    });
  }

  editUser(user: IUser) {

  }

  deleteUser(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this.userService.deleteUser(id).subscribe(result => {
          //+ toastr success message
          this.getAllUsers();
          this.loading = false;
        })
        error => {
          console.log(error);
          this.loading = false;
        }
      }
    });
  }

}
