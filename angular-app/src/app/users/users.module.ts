import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [UsersListComponent],
  imports: [
    CommonModule,
    MatIconModule,
    SharedModule,
    MatProgressSpinnerModule
  ]
})
export class UsersModule { }
